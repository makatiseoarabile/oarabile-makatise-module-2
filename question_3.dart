import 'package:question_3/question_3.dart' as question_3;

void main() {
  var mtnAwards = MtnAwards();
  print("The name of the app is:${mtnAwards.nameOFapp.toUpperCase()}"
      " and it won 1st place for the ${mtnAwards.Category}"
      " in the year ${mtnAwards.YearWonAwards}");
}

class MtnAwards {
  String nameOFapp = "EasyEquities";
  String Category = "Best Consumer Solution";
  int YearWonAwards = 2020;
}
