import 'package:question_2/question_2.dart' as question_2;

void main() {
  var mTNAwardswinners = [
    "FNB Banking App",
    "HealthID (from Discovery)",
    "TransUnion Dealers' Guide",
    "RapidTargets",
    "Matchy",
    "Plascon Inspire Me",
    "phraZapp",
    "iiDENTIFii app",
    "Hellopay SoftPOS",
    "Guardian Health Platform",
    "Ambani Africa",
    "Murimi",
    "Shyft",
    "Sisa",
    "UniWise",
    "Kazi",
    "Takealot app",
    "Rekindle Learning app",
    "Roadsave",
    "Afrihost",
    "Examsta",
    "Checkers Sixty60",
    "Technishen",
    "BirdPro",
    "Lexie Hearing app",
    "GreenFingers Mobile",
    "Xitsonga Dictionary",
    "StokFella",
    "Bottles",
    "Matric Live",
    "My Pregnancy Journey",
    "League of Legends",
    "Scan & Go",
    "EasyEquities",
    "SI Realities",
    "Franc",
    "Naked Insurance",
    "LocTransi",
    "Loot Defence",
    "MoWash",
    "Hydra Farm",
    "Digger",
    "Pineapple",
    "Cowa Bunga",
    "Digemy Knowledge Partner and Besmarter",
    "Bestee",
    "The African Cyber Gaming League App (ACGL)",
    "dbTrack",
    "Difela Hymns",
    "Xander English 1-20",
    "Ctrl",
    "Khula ecosystem",
    "ASI Snakes",
    "iKhokha",
    "HearZA",
    "Tuta-me",
    "KaChing",
    "Friendly Math Monsters",
    "Domestly",
    "Wumdrop",
    "Vula Mobile",
    "DStv Now",
    "CPUT Mobile",
    "M4JAM",
    "EskomSePush",
    "SuperSport",
    "SyncMobile",
    "MyBelongings",
    "LIVE Inspect",
    "VIGO",
    "Zapper",
    "Rea Vaya",
    "Wildlife Tracker",
    "DStv",
    ".comm Telco Data Visualize",
    "PriceCheck Mobile",
    "MarkitShare",
    "Nedbank App Suite",
    "SnapScan",
    "Kids Aid",
    "Bookly",
    "TransUnion 1Check"
        "Gautrain Buddy",
    "OrderIN",
    " EcoSlips",
    " InterGreatMe",
    " Zulzi",
    "Hey Jude",
    " The TreeApp South Africa",
    " WatIf Health Portal",
    " Awethu Project",
    "Pick ‘n Pay’s Super Animals 2",
    " TouchSA",
    " The ORU Social"
  ]; //List is compiled without repeating the names of the app that won more than once
  //The list consits of overall winners of the business academy and overall winners in respective categories

  mTNAwardswinners.sort();

  print("The list of all the MTN Business Awards are as follows:\n");
  print(mTNAwardswinners);

  print("\nThe overall winner of the 2017 business awards was:");
  print(mTNAwardswinners[69]);

  print("\nThe overall winner of the 2018 business awards was:");
  print(mTNAwardswinners[41]);

  print("\nThe total number of winners in the list :");
  print(mTNAwardswinners.length); //Total number of winners in the list
}
